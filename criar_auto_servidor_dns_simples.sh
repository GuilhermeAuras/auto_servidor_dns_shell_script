#!/bin/bash

clear

if [ $# -ne 3 ]; then
      
	echo " "
	echo "Tente usar assim: criar_auto_servidor_dns_simples.sh 'SEU DOMINIO' 'REDE LOCAL INVERTIDA SOMENTE 3 PRIMEIROS OCTETOS' 'IP SERVIDOR DNS'"
	echo "Exemplo: bash criar_auto_servidor_dns_simples.sh curso.com.br 0.17.172 172.17.0.2"
	echo "Suponto que meu dominio seja: curso.com.br e minha rede seja: 172.17.0.0 e meu ip seja: 172.17.0.2"
	echo " "
        exit 1
fi 

DOMINIO=$1
REDE=$2
IP_SERVIDOR=$3
TTL='$TTL'

apt update

if [ $? -eq 0 ]; then

	echo "apt update passou."
	echo " "

else

	echo "apt update nao passou."
	exit 1

fi

apt install bind9 bind9utils bind9-dnsutils bind9-doc bind9-host -y


if [ $? -eq 0 ]; then

	echo "apt install passou."
	echo " "
	echo "Configurando o servidor dns."
	echo " "

else

	echo "apt install nao passou."
	exit 1

fi

cat <<EOF> /etc/bind/named.conf.local
zone "$DOMINIO" {

 type master;
 file "/etc/bind/db-$DOMINIO.conf";

};

zone "$REDE.in-addr.arpa" {
 type master;
 file "/etc/bind/db-$DOMINIO-reverso.conf";
};
EOF

touch /etc/bind/db-$DOMINIO.conf
cat <<EOF> /etc/bind/db-$DOMINIO.conf
$TTL	604800
@	IN	SOA	$HOSTNAME.$DOMINIO. root@$DOMINIO. (

		1	;serial
		604800	;refresh
		86400	;retry
		2419200	;expire
		604800 )	;neative cache TTL
;
@	IN	NS		$HOSTNAME.$DOMINIO.
@	IN	MX	10	$HOSTNAME.$DOMINIO.
$HOSTNAME	IN	A	$IP_SERVIDOR
rancher		IN	A	1.1.1.1
*.rancher	IN	A	2.2.2.2
*.rancher	IN	A	3.3.3.3
EOF

touch /etc/bind/db-$DOMINIO-reverso.conf
cat <<EOF> /etc/bind/db-$DOMINIO-reverso.conf
$TTL	604800
@	IN	SOA	$HOSTNAME.$DOMINIO. root@$DOMINIO. (

		1	;serial
		604800	;refresh
		86400	;retry
		2419200	;expire
		604800 )	;neative cache TTL
;
@	IN	NS	$HOSTNAME.$DOMINIO.
4	IN	PTR	$HOSTNAME.$DOMINIO.
EOF

echo "Servidor dns configurado."
echo " "

echo "Testando o servidor dns."
named-checkzone $DOMINIO /etc/bind/db-$DOMINIO.conf

echo " "
echo "Testando o servidor dns zona reversa."
named-checkzone mail.$HOSTNAME.$DOMINIO /etc/bind/db-$DOMINIO-reverso.conf

echo " "
echo "Se ambos deram ok, rode abaixo:"
echo "/etc/init.d/named restart"
echo "Ou"
echo "/etc/init.d/bind9 restart"
echo " "
echo "Apos reiniciar o named, faca o teste abaixo:"
echo "dig @$IP_SERVIDOR site.rancher.$DOMINIO"
echo " "
