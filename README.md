Baixe o repositorio:<br>
git clone https://gitlab.com/GuilhermeAuras/auto_servidor_dns_shell_script.git<br>
cd auto_servidor_dns_shell_script<br>

Como rodar o script exemplo:<br>
Tente usar assim: criar_auto_servidor_dns_simples.sh 'SEU DOMINIO' 'REDE LOCAL INVERTIDA SOMENTE 3 PRIMEIROS OCTETOS' 'IP SERVIDOR DNS'<br>
Exemplo: bash criar_auto_servidor_dns_simples.sh guilherme.com.br 0.168.192 192.168.0.101<br>
Suponto que meu dominio seja: guilherme.com.br e minha rede seja: 192.168.0.0 e meu ip seja: 192.168.0.101<br>
